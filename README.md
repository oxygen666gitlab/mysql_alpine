Докерфайл который собирает mysql db из alpine базового образа. 
init_db.sh скрипт нужен для запуска бд + инициализации (создание бд пользователя и права на пользователя)
Так же обязателен запуск с параметром skip-networking = 0, хоть в конфиге и не указан skip-networking = 1, почему-то по умолчанию mariadb считает что надо бы блочить все tcp/ip.
Сборка ```docker build -t mysql-alpine .```
Запуск в фоне с мэппингом томов```docker run -p 3306:3306 -e MYSQL_DATABASE=cms_db -v ./mysql-data:/var/lib/mysql mysql-alpine``` 