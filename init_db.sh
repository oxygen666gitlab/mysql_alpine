#!/bin/sh

if [ ! -f /var/lib/mysql/* ]; then
    echo "No MySQL files found. Running mysql_install_db..."
    mysql_install_db --user=mysql --datadir=/var/lib/mysql
fi

# Создание файла script.sql
touch script.sql

# Добавление SQL-команд в файл script.sql
# echo "USE ${MYSQL_DATABASE};" >> script.sql
echo "CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE};" >> script.sql
echo "CREATE USER IF NOT EXISTS '${MYSQL_USER}'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';" >> script.sql
echo "GRANT ALL PRIVILEGES ON *.* TO '${MYSQL_USER}'@'%' WITH GRANT OPTION;" >> script.sql
echo "FLUSH PRIVILEGES;" >> script.sql

# Запуск MySQL в фоновом режиме
echo "!!!!!!!!!!!!!!!!!BAZA DANNIH NAIDENA!!!ZAPUSK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
exec mysqld --datadir=/var/lib/mysql --user=mysql --skip-networking=0 --init-file=/etc/mysql/script.sql

# # Ожидание запуска MySQL
# sleep 10

# Выполнение SQL-команд для создания базы данных и пользователя
# mysql -u root <<EOF
# CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE};
# CREATE USER IF NOT EXISTS '${MYSQL_USER}'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';
# GRANT ALL PRIVILEGES ON *.* TO '${MYSQL_USER}'@'%' WITH GRANT OPTION;
# FLUSH PRIVILEGES;
# EOF

# wait

# mysqld stop 


# set -eox pipefail
# exec "$@"

# RUN echo "#!/bin/bash\nset -e\nruncmd=\"dotnet ${APPNAME}.dll\"\nexec \$runcmd" > /app/publish/entrypoint.sh && chmod +x /app/publish/entrypoint.sh

# что бы запускался pid mysql!!
