FROM alpine:latest

ENV MYSQL_DATABASE=cms_db
ENV MYSQL_USER=mysql
ENV MYSQL_PASSWORD=mysql

WORKDIR /etc/mysql

COPY my.cnf init_db.sh ./


RUN adduser -D -H -u 1000 mysql && apk --no-cache add mysql mysql-client && mkdir -p /run/mysqld /var/lib/mysql && chown -R mysql:mysql /run/mysqld /var/lib/mysql && chmod +x ./init_db.sh

#Если не указать при запуске -v, то волюмес все равно сохранится, но будет по адресу: /var/lib/docker/volumes/kasdkas63216321
VOLUME /var/lib/mysql/$MYSQL_DATABASE/

EXPOSE 3306

ENTRYPOINT ["/bin/sh", "./init_db.sh"]

